import '../styles/index.scss';
// console.log('hello portfolio')

const leftBtn = document.getElementsByClassName('fa-angle-left')[0];
const rightBtn = document.getElementsByClassName('fa-angle-right')[0];

const sliders = document.getElementsByClassName('content__slider');
console.log(sliders)
let indexShow = 0

function addClassHide() { // call when click Back or Next or default
    let index = -1;
    leftBtn.classList.remove('is-hidden');
    rightBtn.classList.remove('is-hidden');

    for (let i = 0; i < sliders.length; i++) {
        const element = sliders[i];
        if (element.classList.contains('is-show')) {
            const child = element.firstElementChild;
            index = i
            setTimeOutClass(child, 'is-opa')
        };
    }

    if (index === 0) leftBtn.classList.add('is-hidden');
    if (index === (sliders.length - 1)) rightBtn.classList.add('is-hidden');
}

function addClassShow(index) {
    for (let i = 0; i < sliders.length; i++) {
        const element = sliders[i];
        element.classList.remove('is-show');
        element.firstElementChild.classList.remove('is-opa');
        if (index === i) element.classList.add('is-show');

        if (element.classList.contains('is-show')) {
            const child = element.firstElementChild;

            setTimeOutClass(child, 'is-opa')
        }
    }
}
function setTimeOutClass(child, className, timing = 100) {
    if (child != null) {
        setTimeout(function () { child.classList.add(className) }, timing);
    }
}
function addListener() {
    if (leftBtn) leftBtn.addEventListener('click', goBack)
    if (rightBtn) rightBtn.addEventListener('click', goNext)
}
function goBack() {
    indexShow--
    addClassShow(indexShow)
    addClassHide()
}

function goNext() {
    indexShow++
    addClassShow(indexShow)
    addClassHide()
}

addClassHide();
addListener();
