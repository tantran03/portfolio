// import { calendarIcon } from '';
const Components = require('../src/script/_components')

const listOfCategories = ['education', 'skill', 'experience', 'projects']

const detailOfCategories = {
    education: {
        title: 'education',
        content: 'education Content'
    },
    skill: {
        title: 'skill',
        content: 'skill Content'
    },
    experience: {
        title: 'experience',
        content: 'experience Content'
    },
    projects: {
        title: 'projects',
        content: 'projects Content'
    },
}

const personalData = [
    {
        heading: 'information',
        subHeadings: [
            { icon: Components.calendarIcon, name: 'birthday' },
            { icon: Components.phoneIcon, name: 'phone' },
            { icon: Components.mailIcon, name: 'email' },
            { icon: Components.gitIcon, name: 'github' },
        ]
    },
    {
        heading: 'soft-skills',
        subHeadings: [
            { icon: 'svg', name: 'english' },
            { icon: 'svg', name: 'team-work' },
            { icon: 'svg', name: 'communication' },
            { icon: 'svg', name: 'self-taught' },
        ]
    },
    {
        heading: 'hobbies',
        subHeadings: [
            { icon: 'svg', name: 'coding' },
            { icon: 'svg', name: 'guitar' },
            { icon: 'svg', name: 'sports' },
        ]
    },
]

module.exports = { listOfCategories, detailOfCategories, personalData }